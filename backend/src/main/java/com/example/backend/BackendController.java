package com.example.backend;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

@RestController
public class BackendController {

    @GetMapping("/status")
    private Map<String, String> backendStatus(){
        return Collections.singletonMap("status", "ok");
    }
}
